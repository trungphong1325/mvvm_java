package com.treeforcom.clone.view.home.student;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.bumptech.glide.Glide;
import com.treeforcom.clone.R;
import com.treeforcom.clone.data.ResourceModel;
import com.treeforcom.clone.data.remote.response.banner.BannerUIModel;
import com.treeforcom.clone.factory.ViewModelFactory;
import java.util.Objects;
import javax.inject.Inject;
import dagger.android.support.AndroidSupportInjection;
public class StudentFragment extends Fragment {
    @Inject
    ViewModelFactory modelFactory;
    private StudentViewModel viewModel;
    private ImageView imgBannerHeaded;
    private ImageView imgBannerFooter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        AndroidSupportInjection.inject(this);
        View rootView = LayoutInflater.from(getContext()).inflate(R.layout.fragment_student, container, false);
        viewModel = ViewModelProviders.of(Objects.requireNonNull(getActivity()), modelFactory).get(StudentViewModel.class);
        mapperView(rootView);
        initializeView();
        return rootView;
    }

    private void mapperView(View rootView) {
        imgBannerHeaded = rootView.findViewById(R.id.imageBannerHeader);
        imgBannerFooter = rootView.findViewById(R.id.imageBannerFooter);
    }

    private void initializeView() {
        int positionHardcode = 4;
        viewModel.getBanner(positionHardcode);
        viewModel.liveDataBanner().observe(Objects.requireNonNull(getActivity()), this::render);
    }

    private void render(ResourceModel<BannerUIModel> resource) {
        switch (resource.statusModel) {
            case LOADING:
                break;
            case SUCCESS:
                if (resource.data != null) {
                    Glide.with(this).load(resource.data.getPhoto()).into(imgBannerHeaded);
                    Glide.with(this).load(resource.data.getPhoto()).into(imgBannerFooter);
                }
                break;
            case ERROR:
                break;
        }
    }
}
