package com.treeforcom.clone.view.home.student;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.treeforcom.clone.data.ResourceModel;
import com.treeforcom.clone.data.remote.response.banner.BannerUIModel;
import com.treeforcom.clone.domain.banner.BannerRepository;
import com.treeforcom.clone.remote.banner.BannerService;

import javax.inject.Inject;

public class StudentViewModel extends ViewModel {
    private BannerRepository bannerRepository;
    private LiveData<ResourceModel<BannerUIModel>> liveDataBanner = new MutableLiveData<>();

    @Inject
    StudentViewModel(BannerService bannerService) {
        bannerRepository = new BannerRepository(bannerService);
    }

    LiveData<ResourceModel<BannerUIModel>> liveDataBanner() {
        return liveDataBanner;
    }

    void getBanner(int position) {
        liveDataBanner = bannerRepository.getBanner(position);
    }
}
