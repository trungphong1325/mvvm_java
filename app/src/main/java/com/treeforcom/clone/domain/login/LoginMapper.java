package com.treeforcom.clone.domain.login;

import com.treeforcom.clone.data.remote.response.login.UserResponse;
import com.treeforcom.clone.data.remote.response.login.UserUIModel;
import com.treeforcom.clone.mapper.IMapper;

public class LoginMapper implements IMapper<UserUIModel, UserResponse> {
    @Override
    public UserUIModel mapToModel(UserResponse type) {
        if (type.getUser() != null) {
            return new UserUIModel(type.getUser().getToken());
        }
        return null;
    }
}

