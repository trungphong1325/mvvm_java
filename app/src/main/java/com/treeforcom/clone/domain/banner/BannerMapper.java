package com.treeforcom.clone.domain.banner;

import com.treeforcom.clone.data.remote.response.banner.BannerResponse;
import com.treeforcom.clone.data.remote.response.banner.BannerUIModel;
import com.treeforcom.clone.mapper.IMapper;

public class BannerMapper implements IMapper<BannerUIModel, BannerResponse> {
    @Override
    public BannerUIModel mapToModel(BannerResponse type) {
        return new BannerUIModel(type.getResult().getPhoto());
    }
}
