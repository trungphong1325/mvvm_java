package com.treeforcom.clone.domain.banner;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import com.treeforcom.clone.data.NetworkBoundSource;
import com.treeforcom.clone.data.ResourceModel;
import com.treeforcom.clone.data.remote.response.banner.BannerResponse;
import com.treeforcom.clone.data.remote.response.banner.BannerUIModel;
import com.treeforcom.clone.mapper.IMapper;
import com.treeforcom.clone.remote.banner.BannerService;
import retrofit2.Call;
public class BannerRepository {
    private BannerService bannerService;
    public BannerRepository(BannerService bannerService) {
        this.bannerService = bannerService;
    }

    public MutableLiveData<ResourceModel<BannerUIModel>> getBanner(int position) {
        return new NetworkBoundSource<BannerResponse, BannerUIModel>() {
            @NonNull
            @Override
            protected Call<BannerResponse> createCall() {
                return bannerService.getBanner(position);
            }

            @NonNull
            @Override
            protected IMapper<BannerUIModel, BannerResponse> mapper() {
                return new BannerMapper();
            }

        }.getAsLiveData();
    }
}
