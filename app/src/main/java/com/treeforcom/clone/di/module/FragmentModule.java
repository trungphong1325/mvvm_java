package com.treeforcom.clone.di.module;

import com.treeforcom.clone.view.home.student.StudentFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class FragmentModule {
    @ContributesAndroidInjector
    abstract StudentFragment contributeStudentFragment();
}
