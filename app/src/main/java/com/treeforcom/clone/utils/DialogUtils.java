package com.treeforcom.clone.utils;

import android.app.Activity;
import android.app.Dialog;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.treeforcom.clone.R;

public class DialogUtils {
    private Activity activity;
    private String message;
    private DialogCallbackListener callbackListener;

    public DialogUtils(Activity activity, String message, DialogCallbackListener callbackListener) {
        this.activity = activity;
        this.message = message;
        this.callbackListener = callbackListener;
    }

    public void showDialog() {
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_error);
        TextView text = dialog.findViewById(R.id.msgError);
        text.setText(message);
        Button btnTryAgain = dialog.findViewById(R.id.tryAgain);
        Button btnClose = dialog.findViewById(R.id.cancel);
        btnClose.setOnClickListener(v -> dialog.cancel());
        btnTryAgain.setOnClickListener(v -> {
            callbackListener.onTryAgain();
            dialog.cancel();
        });
        dialog.show();
    }

}
