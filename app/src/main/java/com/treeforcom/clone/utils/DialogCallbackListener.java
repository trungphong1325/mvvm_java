package com.treeforcom.clone.utils;

public interface DialogCallbackListener {
    void onTryAgain();
}
