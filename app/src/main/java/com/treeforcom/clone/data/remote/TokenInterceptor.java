package com.treeforcom.clone.data.remote;
import android.content.Context;
import com.treeforcom.clone.data.pref.UserManager;
import org.jetbrains.annotations.NotNull;
import java.io.IOException;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class TokenInterceptor implements Interceptor {
    private UserManager userManager;
    public TokenInterceptor(Context context) {
        userManager = new UserManager(context);
    }
    @NotNull
    @Override
    public Response intercept(@NotNull Chain chain) throws IOException {
        Request builder = chain.request();
        builder = builder.newBuilder().addHeader("Authorization", "Bearer " + userManager.getToken()).build();
        return chain.proceed(builder);
    }
}
