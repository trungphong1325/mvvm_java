package com.treeforcom.clone.data;

import androidx.annotation.MainThread;
import androidx.annotation.NonNull;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.MutableLiveData;
import com.treeforcom.clone.mapper.IMapper;
import org.jetbrains.annotations.NotNull;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public abstract class NetworkBoundSource<ResultType, RequestType> {
    private final MediatorLiveData<ResourceModel<RequestType>> result = new MediatorLiveData<>();
    @MainThread
    protected NetworkBoundSource() {
        result.setValue(ResourceModel.loading());
        createCall().enqueue(new Callback<ResultType>() {
            @Override
            public void onResponse(@NotNull Call<ResultType> call, @NotNull Response<ResultType> response) {
                if(response.isSuccessful()){
                    if (response.body() != null) {

                        result.setValue(ResourceModel.success(mapper().mapToModel(response.body())));
                    }
                }else {
                    result.setValue(ResourceModel.error(response.message()));
                }
            }

            @Override
            public void onFailure(@NotNull Call<ResultType> call, @NotNull Throwable t) {
                result.setValue(ResourceModel.error(t.getMessage()));
            }
        });
    }

    @NonNull
    @MainThread
    protected abstract Call<ResultType> createCall();
    @NonNull
    @MainThread
    protected abstract IMapper<RequestType, ResultType> mapper();

    public final MutableLiveData<ResourceModel<RequestType>> getAsLiveData() {
        return result;
    }
}
