package com.treeforcom.clone.data.pref;

import android.content.Context;
import android.content.SharedPreferences;

public class UserManager {
    private final static String NAME_PREF = "name_pref";
    private final static String TOKEN = "token_user";
    private Context context;

    public UserManager(Context context) {
        this.context = context;
    }

    public static void setToken(Context context, String token) {
        SharedPreferences sharedPreferences;
        sharedPreferences = context.getSharedPreferences(NAME_PREF, Context.MODE_PRIVATE);
        sharedPreferences.edit().putString(TOKEN, token).apply();
    }

    public String getToken() {
        SharedPreferences sharedPreferences;
        sharedPreferences = context.getSharedPreferences(NAME_PREF, Context.MODE_PRIVATE);
        return sharedPreferences.getString(TOKEN, "");
    }
}
