package com.treeforcom.clone.data.remote.response.login;

public class UserUIModel {
    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public UserUIModel(String token) {
        this.token = token;
    }

}
