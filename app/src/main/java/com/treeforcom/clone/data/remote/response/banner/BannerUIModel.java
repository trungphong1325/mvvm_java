package com.treeforcom.clone.data.remote.response.banner;

public class BannerUIModel {
    private String photo;

    public BannerUIModel(String photo) {
        this.photo = photo;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }
}
