package com.treeforcom.clone.data.remote.response.banner;

import com.google.gson.annotations.SerializedName;

public class Banner {
    @SerializedName("id")
    private int id;
    @SerializedName("photo")
    private String photo;
    @SerializedName("active")
    private int active;
    @SerializedName("created_at")
    private String created_at;
    @SerializedName("updated_at")
    private String updated_at;
    @SerializedName("position")
    private int position;

    public Banner(int id, String photo, int active, String created_at, String updated_at, int position) {
        this.id = id;
        this.photo = photo;
        this.active = active;
        this.created_at = created_at;
        this.updated_at = updated_at;
        this.position = position;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public int getActive() {
        return active;
    }

    public void setActive(int active) {
        this.active = active;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }
}
