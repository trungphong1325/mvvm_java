package com.treeforcom.clone.data.remote.response.banner;

import com.google.gson.annotations.SerializedName;

public class BannerResponse {
    @SerializedName("result")
    private Banner result;

    public BannerResponse(Banner result) {
        this.result = result;
    }

    public Banner getResult() {
        return result;
    }

    public void setResult(Banner result) {
        this.result = result;
    }
}
