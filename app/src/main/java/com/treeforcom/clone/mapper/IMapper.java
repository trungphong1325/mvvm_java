package com.treeforcom.clone.mapper;

public interface IMapper<V, D> {
    V mapToModel(D type);
}
