package com.treeforcom.clone.remote.banner;

import com.treeforcom.clone.data.remote.response.banner.BannerResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface BannerService {
    @GET("get-banner")
    Call<BannerResponse> getBanner(@Query("position") int position);
}
